import Popover from './Popover.vue'
import {events} from './bus'

let document;

const prepareBinding = (binding) => {
    let {value, arg} = binding
    if (!arg) {
        return {
            name: value
        }
    }
    return {
        name: arg
    }
}

const addClickEventListener = (target, params) => {
    const click = (srcEvent) => {
        events.$emit('show:click', {...params, target, srcEvent})

        let handler = (srcEvent) => {
            events.$emit('hide:click', {...params, target, srcEvent})
            document.removeEventListener('click', handler)
        }

        document.addEventListener('click', handler)
        srcEvent.stopPropagation()
    }

    target.addEventListener('click', click)

    target.$popoverRemoveClickHandlers = () => {
        target.removeEventListener('click', click)
    }
}

export default {
    install(Vue, params = {}) {
        Vue.directive('popover-target', {
            bind: (target, binding, vnode) => {
                document = vnode.context.$root.$el
                let params = prepareBinding(binding)
                addClickEventListener(target, params)
            },
            unbind: (target, binding) => {
                target.$popoverRemoveClickHandlers()
            }
        })
        Vue.component('Popover', Popover)
    }
}

