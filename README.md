### Vue.js popover (no tooltips) - for server side rendering
[Fork from vue-js-popover](https://github.com/euvl/vue-js-popover/)

[Live demo here](http://vue-js-dropdown.yev.io/)

Install:
```bash
npm install vue2-popover --save
```
Import:
```javascript
import Vue      from 'vue'
import Popover  from 'vue2-popover'

Vue.use(Popover)
```
Use 1:
```vue
<button v-popover-target:foo>Toggle popover</button>

<popover name="foo">
  Hello World
</popover>
```
Use 2:
```vue
<template>
    <button v-popover-target="foo">Toggle popover</button>
    
    <popover name="myPopover">
      Hello World
    </popover>
</template>
<script >
    export default {
        data () {
            return{
                foo: 'myPopover'
            }
        }
    }
</script>
```

### Props
```javascript
props: {
  /* Popover name.                           */
  name:    { type: String,  required: true   },
  
  width:   { type: Number,  default: 180     },
  
  /* If set - will show a tiny tip.          */
  pointer: { type: Boolean, default: true    },

  /* set position of pointer (only top/bottom placement)    */  
  pointerX: { type: Number, default: 0.5    },
  
  // Positioning of the popover, relative to the trigger element.	
  placement : { type: String, default: 'bottom'},
  
  stopPropagation: { type: Boolean, default: true },
}
```
### Events
```javascript
    this.$root.$emit('vue2popover::hide','click')
```

### Styling

Popover components have `data-popover="name"` argument that allows to apply styles to it. 

Example:

```
<popover name="foo" :pointer="false">Bar</popover>
```

```css
div[data-popover="foo"] {
  background: #444;
  color: #f9f9f9;

  font-size: 12px;
  line-height: 1.5;
  margin: 5px;
}
```
